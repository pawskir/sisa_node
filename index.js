//Imports
const Parser = require("rss-parser");
const { MongoClient } = require('mongodb');

const dbString = 'mongodb+srv://second_user:myfirstdb@cluster0.txqri.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

//old method/url for collecting all RSSes: http://www.rssmix.com/u/12948204/rss.xml

let UU = 'http://live.webb.uu.se/common/News_RSS?rssTarget=404814&rssTitle=Nyheter%20vid%20informatik%20och%20media&newsDetailSiteNodeId=110660&newsListSiteNodeId=112392&showLeadIn=true#__utma=1.1318108895.1400154343.1507708894.1509691267.57&__utmb=1.4.10.1509691267&__utmc=1&__utmx=-&__utmz=1.1493709162.53.21.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)&__utmv=-&__utmk=149459382';
let UMU = 'https://www.umu.se/institutionen-for-informatik/om-institutionen/nyheter/rssnews?id=7139169';
let GU = 'https://twitr.gq/goteborgsuni/rss';
let SU_DSV = 'http://dsv.su.se/cmlink/nyheter-fr%C3%A5n-dsv-1.117130';
let KAU = 'https://www.kau.se/rss/nyhetsarkiv_rss.xml/informatik/article';
let ORUF = 'https://www.oru.se/funktioner/rss/forskning/';
let ORUN = 'https://www.oru.se/funktioner/rss/nyheter/';
let HIS = 'https://twitr.gq/hogskolanskovde/rss';
let MIUN = 'https://www.miun.se/press/rss/nyheter-rss/';
let JU = 'http://ju.se/4.3728cef415cc55415da13aa3/12.3728cef415cc55415da13aad.portlet?state=rss&sv.contenttype=text/xml;charset=UTF-8';
let HB = 'http://www.hb.se/RSS-HB/';
let LU = 'https://www.lu.se/nyheter-och-press/rss';
let DU = 'http://www.du.se/sv/om-oss/nytt-och-aktuellt/rss---nyheter/';
let LIU = 'https://twitr.gq/liu_indig/rss';
let HV = 'https://twitr.gq/University_West/rss';
let LTU = 'https://www.ltu.se/research/subjects/information-systems/Nyheter-och-aktuellt?mode=rss';
let HH = 'https://twitr.gq/HogskolanHstd/rss';
let MAU = 'https://twitr.gq/malmouni/rss';
let LinneU = 'https://twitr.gq/linneuni/rss';

let rssUrls = [UU, UMU, GU, SU_DSV, KAU, ORUF, ORUN, HIS, MIUN, JU, HB, LU, DU, LIU, HV, LTU, HH, MAU, LinneU];

(async function main() {

    await insertItemsToDb(await selectNewItems(await sortFeedItems(await getEntireFeed(rssUrls))));

})();

//checks if two items are equivalent. returns true or false.
function isEquivalent(a, b) {
    // Create arrays of property names
    let aProps = Object.getOwnPropertyNames(a);
    let bProps = Object.getOwnPropertyNames(b);

    // if number of properties is different, objects are not equivalent
    if (aProps.length != bProps.length) {
        return false;
    }

    for (let i = 0; i < aProps.length; i++) {
        let propName = aProps[i];

        // if values of same property are not equal, objects are not equivalent
        if (a[propName] !== b[propName]) {
            return false;
        }
    }
    // if we made it this far, objects are considered equivalent
    return true;
}

//Filters out equivalent items and sorts in order of date. returns checked list of items.
async function sortFeedItems(feedItems) {
    let items = [];
    // Add the items to the items array
    await Promise.all(feedItems.map(async (currentItem) => {
        try {
            // Add a new item if it doesn't already exist
            if (items.filter((item) => isEquivalent(item, currentItem)).length <= 0) {
                items.push(currentItem);
            }
        } catch (error) {
            console.log(error, 'failed when comparing equivalent items');
        }
    }));

    //date sorting
    items = items.sort((a, b) => (a.isoDate > b.isoDate) ? -1 : 1);
    return items;
}

//Inserts a list of articles into feeds, and logs the result.
async function insertItemsToDb(items) {
    let client = MongoClient | undefined;

    if (items.length > 0) {
        try {
            client = await MongoClient.connect(dbString, { useNewUrlParser: true, useUnifiedTopology: true });
            const database = client.db("sisa_news");
            const feedsCollection = database.collection("feeds");
            const logsCollection = database.collection("logs");
            var result = await feedsCollection.insertMany(JSON.parse(JSON.stringify(items)));
            delete result.ops;
            await logsCollection.insertOne(result);
            console.log('inserted', result.insertedCount, 'items');
        } catch (error) {
            console.log(error, "failed at db insert");
        } finally {
            if (client) {
                await client.close();
            }
        }
    } else {
        console.log('no items to insert');
    }
};

//Reads date and guid from the last item in DB, and then only selects and returns new items.
async function selectNewItems(feedItems) {
    let client = MongoClient | undefined;
    let newItems = [];
    try {
        client = await MongoClient.connect(dbString, { useNewUrlParser: true, useUnifiedTopology: true });
        const database = client.db("sisa_news");
        const feedsCollection = database.collection("feeds");

        //Comparing date and then checks guid
        //TODO: check if 'isodate' doesnt exist in item
        var latestItem = await feedsCollection.find().sort({ isoDate: -1 }).limit(1).toArray();
        if (latestItem.length > 0) {
            try {
                let feedIndex = 0;
                while (Date.parse(latestItem[0].isoDate) <= Date.parse(feedItems[feedIndex].isoDate)) {
                    if (latestItem[0].guid != feedItems[feedIndex].guid) {
                        newItems.push(feedItems[feedIndex]);
                    }
                    feedIndex++;
                }
                console.log('checked', feedIndex - 1, 'new items');
                console.log('selected', newItems.length, 'items');
            } catch (error) {
                console.log(error, 'failed when comparing and selecting new items')
            }
        } else {
            console.log("db col empty, selecting entire feed (", feedItems.length, "items )");
            newItems = feedItems;
        }
    } finally {
        if (client) {
            await client.close();
        }
    }
    return newItems;
}

//Gets the feed from a list of rss-urls, and returns it.
async function getEntireFeed(urls) {

    const parser = new Parser();
    let nrRead = 0;
    let nrError = 0;
    let feed = [];

    await Promise.all(urls.map(async (rssUrl) => {
        try {
            let tempFeed = [];
            tempFeed = await parser.parseURL(rssUrl);
            feed = feed.concat(tempFeed.items);
            nrRead++;
        } catch (error) {
            nrError++;
            console.log(error, 'bad rss or ratelimited?', rssUrl);
        }
    }));

    console.log('Feeds{ Good:', nrRead, 'Bad or ratelimited:', nrError, 'Total:', nrError + nrRead, '}');
    return feed;
}

